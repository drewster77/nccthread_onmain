import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(NCCThread_OnMainTests.allTests),
    ]
}
#endif
