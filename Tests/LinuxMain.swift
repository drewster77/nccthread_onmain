import XCTest

import NCCThread_OnMainTests

var tests = [XCTestCaseEntry]()
tests += NCCThread_OnMainTests.allTests()
XCTMain(tests)
